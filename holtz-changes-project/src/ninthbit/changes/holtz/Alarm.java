package ninthbit.changes.holtz;

import ninthbit.changes.holtz.R;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;

public class Alarm extends BroadcastReceiver {
	
	private static final int EVERY_X_MINUTES = 15;
	public final static String NOTIFICATION_FLAG = "source_notification";
	
	
	@Override
	public void onReceive(final Context context, Intent intent) {
		final Handler handler = new Handler();
		new Thread(){
			public void run(){
				try {
					int before = Preferences.getChangesHash(context);
					ChangesProvider.getChanges(context);
					int after = Preferences.getChangesHash(context);
				if(before != after)
					handler.post(new Runnable() {
						@Override
						public void run() {
							postNotificaion(context);
						}
					});
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}.start();
	}

	private void postNotificaion(Context ctx) {
		
		NotificationCompat.Builder mBuilder =
		        new NotificationCompat.Builder(ctx)
		        .setSmallIcon(R.drawable.icon)
		        .setContentTitle(ctx.getResources().getString(R.string.notificaion_title))
		        .setContentText(ctx.getResources().getString(R.string.notificaion_subsitle))
		        .setTicker(ctx.getResources().getString(R.string.notificaion_ticker))
		        .setAutoCancel(true)
		        .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
		
		Intent resultIntent = new Intent(ctx, MainActivity.class);
		resultIntent.putExtra(NOTIFICATION_FLAG, true);
		
		PendingIntent pIntent = PendingIntent.getActivity(ctx, 0, resultIntent, 0);
		
		mBuilder.setContentIntent(pIntent);
		NotificationManager mNotificationManager =
		    (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);
		mNotificationManager.notify(0, mBuilder.build());
		
	}

	public static void SetAlarm(Context context) {
		if(!Preferences.shouldNotify(context))
			return;
		
		AlarmManager am = (AlarmManager) context
				.getSystemService(Context.ALARM_SERVICE);
		Intent i = new Intent(context, Alarm.class);
		PendingIntent pi = PendingIntent.getBroadcast(context, 0, i, 0);
		int updateEvery = 1000*60*EVERY_X_MINUTES;// once every 15 minutes
		am.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + updateEvery ,updateEvery,pi);
	}

	public static void CancelAlarm(Context context) {
		Intent intent = new Intent(context, Alarm.class);
		PendingIntent sender = PendingIntent
				.getBroadcast(context, 0, intent, 0);
		AlarmManager alarmManager = (AlarmManager) context
				.getSystemService(Context.ALARM_SERVICE);
		alarmManager.cancel(sender);
	}
}