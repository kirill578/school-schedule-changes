package ninthbit.changes.holtz;

public class Change {

	private String mDate;
	private String mText;
	
	public Change(String date,String text){
		mDate = date;
		mText = text;
	}
	
	public String getDate(){
		return mDate;
	}
	
	public String getText() {
		return mText;
	}

}
