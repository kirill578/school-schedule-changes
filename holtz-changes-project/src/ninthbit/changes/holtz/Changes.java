package ninthbit.changes.holtz;

import java.util.ArrayList;

public class Changes {

	private String mUpdateDate;
	private ArrayList<Change> mChanges;

	public Changes(String updateTime,ArrayList<Change> changes){
		mUpdateDate = updateTime;
		mChanges = changes;
	}
	
	public String getLastUpdate(){
		return mUpdateDate;
	}
	
	public ArrayList<Change> getChanges(){
		return mChanges;
	}
	
}
