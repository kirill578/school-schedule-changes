package ninthbit.changes.holtz;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;

public class ChangesFragment extends SwipeFragment implements OnRefreshListener<ListView> {

	private ListAdapter mListAdopter;
	private PullToRefreshListView mListView;
	private TextView mLastUpdate;
	private TextView mClassName;
	
	public static final ChangesFragment newInstance(){
		ChangesFragment f = new ChangesFragment();
		Bundle bd = new Bundle();
		f.setArguments(bd);
		return f;
	}
	
	@Override
	public void onResume() {
		super.onResume();
		
		mListAdopter = new ListAdapter(null, getActivity());
		
		mListView.setAdapter(mListAdopter);
		mListView.setOnRefreshListener(this);
		mListView.getRefreshableView().setSelector(android.R.color.transparent);
		
		mListView.setRefreshing(true);
		refresh();
	}



	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.changes, container, false);
		mListView = (PullToRefreshListView) v.findViewById(R.id.changes_listview);
		mLastUpdate = (TextView) v.findViewById(R.id.last_update);
		mClassName = (TextView) v.findViewById(R.id.class_bottom_bar);
		return v;
	}
	
	private static class ListAdapter extends BaseAdapter {

		private Changes mChanges;
		private Context mContext;

		public ListAdapter(Changes changes, Context c) {
			mChanges = changes;
			mContext = c;
		}
		
		public void updateDataSet(Changes changes){
			mChanges = changes;
			this.notifyDataSetChanged();
		}

		@Override
		public int getCount() {
			if( mChanges != null && mChanges.getChanges().size() != 0)
				return mChanges.getChanges().size();
			else if(mChanges == null)
				return 1;
			else 
				return 1;
		}

		@Override
		public Object getItem(int position) {
			return null;
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if(mChanges == null)
				return new View(mContext);
			
			if(mChanges.getChanges().size() == 0){
					return LayoutInflater.from(mContext).inflate(
						R.layout.listview_item_no_changes, null);
			}
			
			View v = LayoutInflater.from(mContext).inflate(
					R.layout.listview_item, null);
			TextView dateTV = (TextView) v
					.findViewById(R.id.list_view_item_date);
			TextView textTV = (TextView) v
					.findViewById(R.id.list_view_item_text);
			dateTV.setText(mChanges.getChanges().get(position).getDate());
			textTV.setText(mChanges.getChanges().get(position).getText());
			return v;
		}

	}
	
	private void refresh(){
		// EasyTracker.getTracker().sendEvent("Refresh", ChangesProvider.classes.get(classID),"", 0L);

		final Handler handler = new Handler();
		new Thread() {
			public void run() {
				try {
					final Changes changes = ChangesProvider.getChanges(getActivity());
					handler.postDelayed(new Runnable() {
						@Override
						public void run() {
							if(getActivity() != null) {
								mListView.onRefreshComplete();
								mListAdopter.updateDataSet(changes);
								setLastUpdate(changes.getLastUpdate());
								setClassBottomBarText(Preferences.getClassName(getActivity()));
							}
						}
					},2000);
				} catch (Exception e) {
					handler.post(new Runnable() {
						@Override
						public void run() {
							// TODO connection error
						}
					});
				}
			}
		}.start();
	}
	
	private void setLastUpdate(String lastUpdate){
		mLastUpdate.setText(lastUpdate);
	}
	
	private void setClassBottomBarText(String classText){
		mClassName.setText(classText);
	}

	@Override
	public void onRefresh(PullToRefreshBase<ListView> refreshView) {
		refresh();
	}

	@Override
	public String getFragmentName() {
		return getActivity().getString(R.string.tab_cha);
	}

	@Override
	public TextView getTabTextView() {
		return (TextView) getActivity().findViewById(R.id.textview_changes_header);
	}
	
}
