package ninthbit.changes.holtz;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;

public class ChangesProvider {

	public static Changes getChanges(Context ctx) throws Exception {
		ArrayList<Change> changes = new ArrayList<Change>();
		
		
		URL url = new URL("http://8.holtz-api.appspot.com/school/changes/" + Preferences.getSchoolID(ctx) + "/" + Preferences.getClassID(ctx));
        URLConnection connection = url.openConnection();
        connection.connect();
        
        // download the file
        InputStream input = new BufferedInputStream(url.openStream());
        BufferedReader br = new BufferedReader(new InputStreamReader(input));
        
        StringBuilder sb = new StringBuilder();
        
        String line;
    	while ((line = br.readLine()) != null) {
    		sb.append(line);
    	} 
 
        input.close();
        
        changes.clear();
        
        String jsonString = sb.toString();
        
        
        JSONObject jData = new JSONObject(jsonString);
        
        JSONArray jObject = jData.getJSONArray("changes");
        for (int j = 0; j < jObject.length(); j++) {
        	JSONArray jChange = (JSONArray) jObject.getJSONArray(j);
        	changes.add(new Change(jChange.getString(0), jChange.getString(1)));
		}
        
        Preferences.setChangesHash(ctx, jData.getString("changes").hashCode());
        
		return new Changes(jData.getString("last-update"), changes);
	}
	
}
