package ninthbit.changes.holtz;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

public class HelperFunctions {

	public static View createListViewMessage(Context ctx,String title,String subtitle){
		View v = LayoutInflater.from(ctx).inflate(
				R.layout.listview_msg, null);

		TextView tv_title = (TextView) v.findViewById(R.id.list_view_item_msg);
		TextView tv_subtitle = (TextView) v.findViewById(R.id.list_view_item_msg_subtitle);
		
		tv_title.setText(title);
		if(subtitle != null)
			tv_subtitle.setText(subtitle);
		else
			tv_subtitle.setVisibility(View.GONE);
		
		return v;
	}
	
	public static View createListViewMessage(Context ctx,int title,int subtitle){
		String sTitle = ctx.getString(title);
		String sSubtitle = null;
		if(subtitle != 0)
			sSubtitle = ctx.getString(subtitle);
		return createListViewMessage(ctx, sTitle, sSubtitle);
	}
	
}
