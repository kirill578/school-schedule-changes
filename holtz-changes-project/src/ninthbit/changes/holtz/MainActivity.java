package ninthbit.changes.holtz;

import java.util.GregorianCalendar;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import com.google.analytics.tracking.android.EasyTracker;

public class MainActivity extends FragmentActivity {

	private SwipeFragment[] fragments = new SwipeFragment[] { TimeTableFragment.newInstance(),ChangesFragment.newInstance() };
	private ViewPager mViewPager;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_main);
		
		AdView adView = new AdView(this, AdSize.SMART_BANNER, "a1518a9f03b68a7");
		FrameLayout rl = (FrameLayout) findViewById(R.id.ad_holder);
		rl.addView(adView);
		adView.loadAd(new AdRequest().setBirthday(new GregorianCalendar(1996, 1, 1)));

		EasyTracker.getInstance().setContext(this.getApplicationContext());

		
		if(Preferences.getSchoolID(this) == null || Preferences.getClassID(this) == null){
			this.onClickMore(null);
		}
		
		mViewPager = (ViewPager) findViewById(R.id.my_list_fragment);
		mViewPager.setOffscreenPageLimit(5);
        MyFragmentPagerAdapter mMyFragmentPagerAdapter = new MyFragmentPagerAdapter(getSupportFragmentManager(),fragments);
        mViewPager.setAdapter(mMyFragmentPagerAdapter);
        mViewPager.setOnPageChangeListener(mMyFragmentPagerAdapter);
        mViewPager.setCurrentItem(1);
		
	}
	
	public void onClickTimeTable(View v){
		mViewPager.setCurrentItem(0);
	}
	
	public void onClickChanges(View v){
		mViewPager.setCurrentItem(1);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		TextView tv = (TextView) findViewById(R.id.acb_title);
		String schoolName = Preferences.getSchoolName(this);
		tv.setText(schoolName == null ? "לא הוגדר" : schoolName);
	}
	
	
	private static class MyFragmentPagerAdapter extends FragmentPagerAdapter implements OnPageChangeListener {

        private SwipeFragment[] mFragments;

		public MyFragmentPagerAdapter(FragmentManager fm,SwipeFragment[] f) {
             super(fm);
             mFragments = f;
        }  

        @Override
        public Fragment getItem(int index) { 
             return mFragments[index];
        }  

        @Override 
        public int getCount() {
             return mFragments.length;
        }

		@Override
		public void onPageSelected(int location) {
			for (SwipeFragment frag : mFragments) 
				frag.setSelected(false);
			mFragments[location].setSelected(true);
		}
        
		@Override
		public void onPageScrollStateChanged(int arg0) {
		}

		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2) {
		}


		
   }
	
	@Override
	protected void onStart(){
		super.onStart();
		
		if(Preferences.getClassName(this) != null && Preferences.getSchoolName(this) != null)
			EasyTracker.getTracker().sendView(Preferences.getSchoolName(this) + "/" + Preferences.getClassName(this));
		
		Alarm.CancelAlarm(this.getApplicationContext());
	}
	
	@Override
	protected void onStop(){
		super.onStop();
		Alarm.SetAlarm(this.getApplicationContext());
	}
	
	public void onClickMore(View v){
		startActivity(new Intent(this,MenuActivity.class));
	}

}
