package ninthbit.changes.holtz;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

public class MenuActivity extends Activity implements OnCheckedChangeListener {

	private String[] items;
	private String[] itemsClasses;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);;
		setContentView(R.layout.settings);
		setClassLoading();
		setSchoolLoading();
		setUpSchoolSelection();
		setUpNotification();
		findViewById(R.id.settings_ok_btn).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});
	}
	
	@Override
	public void onBackPressed() {
		if( Preferences.getSchoolID(this) == null ) {
			Toast.makeText(this, "נא לבחור בית ספר", Toast.LENGTH_SHORT).show();
		} else if (Preferences.getClassID(this) == null ){
			Toast.makeText(this, "נא לבחור כיתה", Toast.LENGTH_SHORT).show();
		} else {
			finish();
		}
	}
	
	private void setUpNotification(){
		boolean notificationEnabled = Preferences.shouldNotify(this);
		
		CheckBox cb = (CheckBox) findViewById(R.id.checkBox1);
		cb.setOnCheckedChangeListener(this);
		cb.setChecked(notificationEnabled);
		
	}
	
	public void ok(View v){
		finish();
	}
	
	private JSONArray schoolsJson;
	
	private void setUpSchoolSelection(){
		final TextView spinner = (TextView) findViewById(R.id.school);
		setSpinnerLoading(spinner);
		Volley.newRequestQueue(this).add(new JsonArrayRequest("http://8.holtz-api.appspot.com/school/list", new Listener<JSONArray>() {
			@Override
			public void onResponse(JSONArray response) {
				schoolsJson = response;
				String currentSchoolID = Preferences.getSchoolID(getApplicationContext());
				
				ArrayList<String> list = new ArrayList<String>();
				int selected = -1;
				for (int i = 0; i < schoolsJson.length(); i++) {
					try {
						String name = schoolsJson.getJSONObject(i).getString("name");
						String id = schoolsJson.getJSONObject(i).getString("id");
						list.add(name);
						if(id.equals(currentSchoolID))
							selected = i;
					} catch (JSONException e) {
					}
				}
				
				items = list.toArray(new String[list.size()]);
				
				if(selected != -1) {
					setUpClassSelection(); // in case school is no longer supported
					spinner.setText(items[selected]);
				} else {
					// force selection
					spinner.setText("בחר בית ספר");
				}
				
				spinner.setEnabled(true);
				spinner.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						AlertDialog.Builder ad = new AlertDialog.Builder(MenuActivity.this);
						ad.setItems(items, new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								try {
									String name = schoolsJson.getJSONObject(which).getString("name");
									spinner.setText(name);
									String id = schoolsJson.getJSONObject(which).getString("id");
									Preferences.setSchoolID(getApplicationContext(), id);
									Preferences.setSchoolName(getApplicationContext(), name);
									Preferences.setClassID(getApplicationContext(), null);
									Preferences.setClassName(getApplicationContext(), null);
									setUpClassSelection();
								} catch (JSONException e) {
									Toast.makeText(getApplicationContext(), "ERROR", Toast.LENGTH_SHORT).show();
								}
							}
						});
						ad.show();
					}
				});
			}
		}, new ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				spinner.setText("שגיאה - לחץ כאן כדי לנסות שוב");
				spinner.setEnabled(true);
				spinner.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						setUpSchoolSelection();
					}
				});
			}
		}));
	}
	
	
	private void setClassLoading(){
		setSpinnerLoading((TextView) findViewById(R.id.clazz));
	}
	
	private void setSchoolLoading(){
		setSpinnerLoading((TextView) findViewById(R.id.school));
	}
	
	private void setSpinnerLoading(TextView s){
		s.setText("טוען");
		s.setEnabled(false);
	}
	
	private JSONArray classesJson;
	
	protected void setUpClassSelection() {
		final TextView spinner =  (TextView) findViewById(R.id.clazz);
		setClassLoading();
		String id = Preferences.getSchoolID(getApplicationContext());
		Volley.newRequestQueue(this).add(new JsonArrayRequest("http://8.holtz-api.appspot.com/school/classes/" + id, new Listener<JSONArray>() {
			@Override
			public void onResponse(JSONArray response) {
				classesJson = response;
				
				String currentClassID = Preferences.getClassID(getApplicationContext());
				
				ArrayList<String> list = new ArrayList<String>();
				int selected = -1;
				for (int i = 0; i < classesJson.length(); i++) {
					try {
						String name = classesJson.getJSONObject(i).getString("name");
						String id = classesJson.getJSONObject(i).getString("ID");
						list.add(name);
						if(id.equals(currentClassID))
							selected = i;
					} catch (JSONException e) {
					}
				}
				
				itemsClasses = list.toArray(new String[list.size()]);
				
				if(selected != -1) {
					spinner.setText(itemsClasses[selected]);
				} else {
					spinner.setText("בחר כיתה");
				}
				
				spinner.setEnabled(true);
				spinner.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						AlertDialog.Builder ad = new AlertDialog.Builder(MenuActivity.this);
						ad.setItems(itemsClasses, new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								try {
									String name = classesJson.getJSONObject(which).getString("name");
									spinner.setText(name);
									String id = classesJson.getJSONObject(which).getString("ID");
									Preferences.setClassID(getApplicationContext(), id);
									Preferences.setClassName(getApplicationContext(), name);
								} catch (JSONException e) {
									Toast.makeText(getApplicationContext(), "ERROR", Toast.LENGTH_SHORT).show();
								}
							}
						});
						ad.show();
					}
				});
				
			}
		}, new ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				spinner.setText("שגיאה - לחץ כאן כדי לנסות שוב");
				spinner.setEnabled(true);
				spinner.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						setUpClassSelection();
					}
				});
			}
		}));
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		Preferences.setShouldNotify(this, isChecked);
	}

}
