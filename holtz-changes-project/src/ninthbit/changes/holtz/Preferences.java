package ninthbit.changes.holtz;

import android.content.Context;
import android.content.SharedPreferences;

public class Preferences {

	public static String Preferences_File = "preferences_new";
	
	public static String SCHOOL_ID = "school";
	public static String SCHOOL_NAME = "school_name";
	public static String CLASS = "class";
	public static String CLASS_NAME = "class_name";
	public static String NOTIFY = "notify";
	public static String HASH = "HASH";
	
	private static SharedPreferences getSharedPreferences(Context ctx){
		return ctx.getSharedPreferences(Preferences_File,Context.MODE_PRIVATE);
	}
	
	public static String getClassID(Context ctx){
		return getSharedPreferences(ctx).getString(CLASS, null);	
	}
	
	public static void setClassID(Context ctx,String classID){
		getSharedPreferences(ctx).edit().putString(CLASS, classID).commit();
	}
	
	
	public static String getSchoolID(Context ctx){
		return getSharedPreferences(ctx).getString(SCHOOL_ID, null);	
	}
	
	public static void setSchoolID(Context ctx,String schoolID){
		getSharedPreferences(ctx).edit().putString(SCHOOL_ID, schoolID).commit();
	}
	
	public static String getSchoolName(Context ctx){
		return getSharedPreferences(ctx).getString(SCHOOL_NAME, null);	
	}
	
	public static void setSchoolName(Context ctx,String schoolName){
		getSharedPreferences(ctx).edit().putString(SCHOOL_NAME, schoolName).commit();
	}
	
	
	public static boolean shouldNotify(Context ctx){
		return getSharedPreferences(ctx).getBoolean(NOTIFY, true);
	}
	
	public static void setShouldNotify(Context ctx,boolean shouldNotify){
		getSharedPreferences(ctx).edit().putBoolean(NOTIFY, shouldNotify).commit();
	}
	
	public static int getChangesHash(Context ctx){
		return getSharedPreferences(ctx).getInt(HASH, 0);
	}
	
	public static void setChangesHash(Context ctx,int hash){
		getSharedPreferences(ctx).edit().putInt(HASH, hash).commit();
	}
	
	public static String getClassName(Context ctx) {
		return getSharedPreferences(ctx).getString(CLASS_NAME, null);
	}
	
	public static void setClassName(Context ctx,String name){
		getSharedPreferences(ctx).edit().putString(CLASS_NAME, name).commit();
	}
	
}
