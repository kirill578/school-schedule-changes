package ninthbit.changes.holtz;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.widget.TextView;

public abstract class SwipeFragment extends Fragment {

	private boolean mSelected = false;
	public int selectedColor;
	public int notSelectedColor;
	
	public abstract String getFragmentName();
	
	public abstract TextView getTabTextView();

	public void setSelected(boolean selected){
		mSelected = selected;
		
		if(getActivity() != null ){
			TextView tv = getTabTextView();
			tv.setText(getFragmentName());
			if(mSelected)
				tv.setTextColor(selectedColor);
			else
				tv.setTextColor(notSelectedColor);
		}
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		selectedColor = activity.getResources().getColor(R.color.tab_selected);
		notSelectedColor = activity.getResources().getColor(R.color.tab_not_selected);
		setSelected(mSelected);
	}

}