package ninthbit.changes.holtz;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collections;

import ninthbit.changes.holtz.TimeTable.Cell.Lesson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

public class TimeTable {
	
	private ArrayList<ArrayList<Cell>> mDays;
	
	public static TimeTable getTimeTable(Context ctx) throws Exception {
		TimeTable table = new TimeTable();
		
		URL url = new URL("http://8.holtz-api.appspot.com/school/timetable/" + Preferences.getSchoolID(ctx) + "/" + Preferences.getClassID(ctx));
        URLConnection connection = url.openConnection();
        connection.connect();
        
        // download the file
        InputStream input = new BufferedInputStream(url.openStream());
        BufferedReader br = new BufferedReader(new InputStreamReader(input));
        
        StringBuilder sb = new StringBuilder();
        
        String line;
    	while ((line = br.readLine()) != null) {
    		sb.append(line);
    	} 
 
        input.close();
        
        String jsonString = sb.toString();
        
        JSONArray jData = new JSONArray(jsonString);
        
        for (int i = 0; i < 6; i++) {
			ArrayList<Cell> day = table.mDays.get(i);
			JSONArray cells = jData.getJSONObject(i).getJSONArray("cells");
			for (int j = 0; j < cells.length(); j++) {
				try {
					JSONObject cell = cells.getJSONObject(j);
					ArrayList<Lesson> ls = new ArrayList<TimeTable.Cell.Lesson>();
					JSONArray lessons = cell.getJSONArray("lessons");
					for (int k = 0; k < lessons.length(); k++) {
						JSONObject singleLesson = lessons.getJSONObject(k);
						ls.add(new Lesson(singleLesson.getString("name"), singleLesson.getString("teacher")));
					}
					day.add(j,new Cell(j,cell.getString("time"), ls));
				} catch (JSONException e){
					day.add(j, null);
				}
			}
		}
		
		return table;
	}
	
	private TimeTable(){
		mDays = new ArrayList<ArrayList<Cell>>();
		for (int i = 0; i < 6; i++)
			mDays.add(new ArrayList<TimeTable.Cell>());
	}
	
	public ArrayList<Cell> getDay(Day d){
		ArrayList<Cell> day = mDays.get(d.ordinal());
		day.removeAll(Collections.singleton(null));
		return day;
	}
	
	public static class Cell {
		
		private String mTime;
		private int mCellNumber;
		private ArrayList<Lesson> mLessons;
		
		public Cell(int number,String time,ArrayList<Lesson> lessons){
			mCellNumber = number;
			mTime = time;
			mLessons = lessons;
		}
		
		public String getTime(){
			return mTime;
		}
		
		public ArrayList<Lesson> getLessons(){
			return mLessons;
		}
		
		public int getCellNumber(){
			return mCellNumber;
		}
		
		public static class Lesson {
			
			public Lesson(String name,String teacher){
				this.name = name;
				this.teacher = teacher;
			}
			
			public String name;
			public String teacher;
		}
		
	}

}
