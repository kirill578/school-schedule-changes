package ninthbit.changes.holtz;


import java.util.ArrayList;
import java.util.Calendar;

import ninthbit.changes.holtz.TimeTable.Cell.Lesson;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;

public class TimeTableFragment extends SwipeFragment implements OnRefreshListener<ListView> {

	public static final TimeTableFragment newInstance(){
		TimeTableFragment f = new TimeTableFragment();
		Bundle bd = new Bundle();
		f.setArguments(bd);
		return f;
	}

	private PullToRefreshListView mListView;
	private ListAdapter mListAdopter;
	
	@Override
	public void onResume() {
		super.onResume();
		
		mListAdopter = new ListAdapter(getActivity());
		mListView.setAdapter(mListAdopter);
		mListView.setOnRefreshListener(this);
		mListView.setRefreshing(true);
		
		mDaysButton = new ArrayList<TextView>();
		
		setupButton(getView(),Day.SUNDAY,   R.id.time_table_button_sunday);
		setupButton(getView(),Day.MONDAY,   R.id.time_table_button_monday);
		setupButton(getView(),Day.TUESDAY,  R.id.time_table_button_tuesday);
		setupButton(getView(),Day.WEDNESDAY,R.id.time_table_button_wednesday);
		setupButton(getView(),Day.THURSDAY, R.id.time_table_button_thursday);
		setupButton(getView(),Day.FRIDAY,   R.id.time_table_button_friday);
		
		// Sunday is 0
		int currentDay = Calendar.getInstance().get(Calendar.DAY_OF_WEEK) - 1;
		if(currentDay < mDaysButton.size()){ // we don't have time table on Saturday
			mDaysButton.get(currentDay).setTextColor(getResources().getColor(R.color.time_table_day_selected));
			mListAdopter.setDate(Day.values()[currentDay]);
		} else {
			mDaysButton.get(0).setTextColor(getResources().getColor(R.color.time_table_day_selected));
			mListAdopter.setDate(Day.SUNDAY);
		}
			
		
	}

	private ArrayList<TextView> mDaysButton;
	
	private void setupButton(View prerent,final Day day,int id){
		TextView view = (TextView) prerent.findViewById(id);
		mDaysButton.add(view);
		view.setTextColor(getResources().getColor(R.color.time_table_day_not_selected));
		view.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				mListAdopter.setDate(day);
				AlphaAnimation aa = new AlphaAnimation(0, 1);
				aa.setDuration(300);
				mListView.getRefreshableView().startAnimation(aa);
				for (TextView tv : mDaysButton)
					tv.setTextColor(getResources().getColor(R.color.time_table_day_not_selected));
				((TextView) v).setTextColor(getResources().getColor(R.color.time_table_day_selected));
			}
		});
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.time_table, container, false);
		mListView = (PullToRefreshListView) v.findViewById(R.id.table_listview);
		mListView.getRefreshableView().setSelector(android.R.color.transparent);
		return v;
	}
	
	
	
	private static class ListAdapter extends BaseAdapter {

		private Context mContext;
		private TimeTable mTimeTable;
		private Day mDay;
		private boolean mError = false;

		public ListAdapter(Context c) {
			mContext = c;
			mDay = Day.SUNDAY;
		}
		
		public void setError(){
			mError = true;
			this.notifyDataSetChanged();
		}
		
		public void setDate(Day day){
			mDay = day;
			this.notifyDataSetChanged();
		}
		
		public void updateDataSet(TimeTable data){
			mTimeTable = data;
			mError = false;
			this.notifyDataSetChanged();
		}

		@Override
		public int getCount() {
			if( mTimeTable != null && mTimeTable.getDay(mDay) != null && mTimeTable.getDay(mDay).size() != 0) {
				int count = mTimeTable.getDay(mDay).size();
				// all the last classes are not free classes, puiples just go home
				while( count > 0 && mTimeTable.getDay(mDay).get(--count).getLessons().size() == 0);
				count++; // count is one above the last index
				return count;
			} else if(mTimeTable == null)
				return 1;
			else 
				return 1;
		}

		@Override
		public Object getItem(int position) {
			return null;
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}
		
		public boolean isAllFreeLeasons() {
			for (int i = 0; i < mTimeTable.getDay(mDay).size(); i++) {
				if ( mTimeTable.getDay(mDay).get(i).getLessons().size() != 0)
					return false;
			}
			return true;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if(mError)
				return HelperFunctions.createListViewMessage(mContext,R.string.connection_error,R.string.connection_error_ins);
			
			if(mTimeTable == null)
				return new View(mContext);
			
			
			if(isAllFreeLeasons())
				return HelperFunctions.createListViewMessage(mContext,R.string.no_time_table,0);
			
			
			View v = LayoutInflater.from(mContext).inflate(
					R.layout.listview_item_table_cell, null);
			
			TextView time = (TextView) v
					.findViewById(R.id.cell_time);
			TextView number = (TextView) v
					.findViewById(R.id.cell_number);
			LinearLayout holder = (LinearLayout) v
					.findViewById(R.id.lesson_holder);
			
			
			time.setText(mTimeTable.getDay(mDay).get(position).getTime());
			number.setText(Integer.toString(mTimeTable.getDay(mDay).get(position).getCellNumber()));
			
			
			if( mTimeTable.getDay(mDay).get(position).getLessons().size() == 0 ) {
				View l = LayoutInflater.from(mContext).inflate(R.layout.listview_item_table_free_lesson, null);
				holder.addView(l);
			}
			
			for (Lesson lesson : mTimeTable.getDay(mDay).get(position).getLessons()) {
				View l = LayoutInflater.from(mContext).inflate(
						R.layout.listview_item_table_lesson, null);
				TextView name = (TextView) l
						.findViewById(R.id.lesson_name);
				TextView teacher = (TextView) l
						.findViewById(R.id.lesson_teacher);

				name.setText(lesson.name);
				teacher.setText(lesson.teacher);
				
				holder.addView(l);
			}
			
			return v;
		}

	}
	
	private void refresh(){
		final Handler handler = new Handler();
		new Thread() {
			public void run() {
				try {
					final TimeTable tt = TimeTable.getTimeTable(getActivity());
					handler.post(new Runnable() {
						@Override
						public void run() {
							mListView.onRefreshComplete();
							mListAdopter.updateDataSet(tt);
						}
					});
				} catch (Exception e) {
					e.printStackTrace();
					handler.post(new Runnable() {
						@Override
						public void run() {
							mListView.onRefreshComplete();
							mListAdopter.setError();
						}
					});
				}
			}
		}.start();
	}

	@Override
	public void onRefresh(PullToRefreshBase<ListView> refreshView) {
		refresh();
	}

	@Override
	public String getFragmentName() {
		return getActivity().getString(R.string.tab_sch);
	}
	
	@Override
	public TextView getTabTextView() {
		return (TextView) getActivity().findViewById(R.id.textview_sche_header);
	}
	
}
